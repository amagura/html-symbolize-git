#!/usr/bin/ruby 

file_a = File.open("xhsz.py", "r");
file_b = File.open("hsz.py", "r");
file_c = File.open("unhsz.py", "r");

file_a.each_line do |line|

    if line =~ /version_ =/

	$version_a = line.sub(/\sversion_ = '/, '').sub(/'/, '')

    end

end

file_a.close

puts $version_a

file_b.each_line do |line|

    if line =~ /version =/

	$version_b = line.sub(/version = '/, '').sub(/'/, '')

    end

end

file_b.close

puts $version_b

file_c.each_line do |line|

    if line =~ /version =/ 

	$version_c = line.sub(/version = '/, '').sub(/'/, '')

    end
    
end

file_c.close

puts $version_c

if not (($version_a == $version_b) and ($version_a == $version_c)) 

    $stderr.puts "files have different versions"

    exit 1

else

    puts "files are all from same version"

    exit 0 

end


