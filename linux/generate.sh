#!/bin/sh

cat << EOF

all : build

build :
	cd src ;\
	    make all -B
	mv xhsz.py bin/xhsz
clean : 
	mv bin/xhsz xhsz.py
	\$(RM) hsz
	\$(RM) unhsz 
	\$(RM)r bin
	#
	cd src ;\
	    make clean -B

install : 
	install -d \$(DESTDIR)/usr/{bin,share/man/man1,share/doc/hsz}
	# 
	for files in \$\$(ls -1 docs/) ;\
	do \
	    install -m 644 -t \$(DESTDIR)/usr/share/doc/hsz docs/\$\$files ;\
	done 
	#
	for files in \$\$(ls -1 man/) ;\
	do \
	    install -m 644 -t \$(DESTDIR)/usr/share/man/man1 man/\$\$files ;\
	done
	#
	for files in \$\$(ls -1 bin/) ;\
	do \
	    install -m 755 -t \$(DESTDIR)/usr/bin bin/\$\$files ;\
	done

EOF
