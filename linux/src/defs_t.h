/****
    (filename)
    
    defs_t.h

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.


****/


#ifndef DEFS_T_H_GUARD
#define DEFS_T_H_GUARD 1

#undef BEGIN_C_DECLS 
#undef END_C_DECLS
#ifdef __cplusplus
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
# define BEGIN_C_DECLS /* empty */
# define END_C_DECLS /* empty */
#endif 

#undef PARAMS
#if defined (__STDC__) || defined (_AIX) \
    || (defined (__mips) && defined (_SYSTYPE_SVR4))\
    || defined(WIN32) || defined(__cplusplus)
# define PARAMS(protos) protos
#else
# define PARAMS(protos) ()
#endif

BEGIN_C_DECLS 

struct defs_t
{
    int debug;
    char* program_name;
    char* behavior;
    char* tc_msg; // try-catch_message
    char* inversion; // hsz->unhsz, unhsz->hsz
    char* inverba; // just a quick-fix... FIXME, I need to be replaced with something more sophisticated and robust/flexible.  
} defs;

END_C_DECLS

#endif /* DEFS_T_H_GUARD */
