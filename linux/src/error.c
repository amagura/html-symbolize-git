/****
    (filename)
    
    error.c

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
	
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.

****/
#include <stdio.h> //------- fprintf()
#include <stdlib.h> //------ exit()
#include "defs_t.h" //------ defs.program_name
#include "error.h" //------- self

void error(char section, int part)
{
    switch (section)
    {
	/* section A is limited to the locality of main() */
	case 'A':

	    switch (part)
	    {
		/* part 2 is the part nearest TOP-LEVEL, which is where main is first declared.

		   NOTE, parts 0 and 1 are not used since these are the values for 'true' and 'false'. */

		case 2:
		    fprintf(stderr, "%s: missing string operand\n", defs.program_name);
		    exit (2); // incorrect command-line usage

		/* optopt > 0 */
		case 3:
		    exit (2); // no need to print an error message--getopt should print one for us. 

		case 4:
		    fprintf(stderr, "%s: improper usage: see %s for %sing\n", defs.program_name, defs.inversion, defs.inverba); // FIXME, I require too much redundant overhead.
		    exit(1);

	    }
	    break;

	case 'F':

	    switch (part)
	    {
		case 0:
		    fprintf(stderr, "%s: error: Invalid argument on: %s\n", defs.program_name, defs.tc_msg);
		    exit(1);
	    }
	    break;
    }
}


