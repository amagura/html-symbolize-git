/****
    (filename)
    
    help.c

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.


****/
#include "help.h"   //-------- self
#include "defs_t.h" //-------- defs.program_name, defs.behavior
#include <stdio.h> //--------- printf()

void help()
{

    printf("usage: %s [options] string\n", defs.program_name);
    
    printf("\n");
    
    printf("  -h, --help\tprint this message and exit\n");
    
    printf("      --version\tprint version and exit\n");
    
    printf("      --debug\tprint program internals while running\n");
    
    printf("\nTo %se a string that starts with a '-', use one of these methods:\n", defs.behavior);
    
    printf("\n  %s -- -foobar", defs.program_name);
    
    printf("\n  %s -- --foobar\n", defs.program_name);
    
    printf("\nIf you have problems %sing a string that contains special characters, for example '^', try using one of these methods:\n", defs.behavior);
    
    printf("\n  %s foo\\^bar", defs.program_name);
    
    printf("\n  %s 'foo^bar'", defs.program_name);

    printf("\n  %s \"foo^bar\"\n", defs.program_name);

}
