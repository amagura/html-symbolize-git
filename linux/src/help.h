/****
    (filename)
    
    help.h

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.


****/
#ifndef HELP_H_GUARD
#define HELP_H_GUARD 1

/* BEGIN_C_DECLS should be used at the beginning of your declarations, 
 * so that C++ compilers don't mangle their names.
 * Use END_C_DECLS at the end of C declarations. 
 * 
 ***/

#undef BEGIN_C_DECLS 
#undef END_C_DECLS
#ifdef __cplusplus
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
# define BEGIN_C_DECLS /* empty */
# define END_C_DECLS /* empty */
#endif 

/* PARAMS is a macro used to wrap function prototypes,
 * so that compilers that don't understand ANSI C
 * prototypes still work, and ANSI C compilers can issue
 * warnings about type mismatches.  
 ***/

#undef PARAMS
#if defined (__STDC__) || defined (_AIX) \
    || (defined (__mips) && defined (_SYSTYPE_SVR4))\
    || defined(WIN32) || defined(__cplusplus)
# define PARAMS(protos) protos
#else
# define PARAMS(protos) ()
#endif

BEGIN_C_DECLS 

void help PARAMS((void));

END_C_DECLS

#endif /* HELP_H_GUARD */
