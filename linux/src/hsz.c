/****
    (filename)
    
    hsz.c

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.

****/

// system includes
#include <stdio.h> //---------- printf()
#include <getopt.h> //--------- getopt_long()
#include <stdbool.h> //-------- true, false
#include <string.h> //--------- strlen()
#include <stdlib.h> //--------- free()

// local includes
#include "version.h" //-------- version
#include "help.h"  //---------- help()
#include "error.h" //---------- error()
#include "defs_t.h" //--------- defs.debug, defs.program_name
#include "opts.h" //----------- short_opts, long_opts[]

int main(int argc, char** argv)
{
    if (defs.debug)
    {
	printf("argc: %d\n", argc);
	printf("optind: %d\n", optind);
    }
    
    
    /* Initialize defs_t */
    defs.debug = false;
    //defs.verbose = false;
    
    /* splint found a few memory leaks, this here addresses those leaks */
    free(defs.program_name); 
    free(defs.behavior);
    free(defs.tc_msg);
    free(defs.inversion);
    free(defs.inverba);

    defs.program_name = argv[0]; // allows global access to program name
    defs.behavior = "encod";
    defs.tc_msg = NULL;
    defs.inversion = "unhsz";
    defs.inverba = "decod";

    /* variable for character storage */

    int c;
    
    /* opt counter, I believe */

    int* optc = 0;

    /* process args */

    while ((c = getopt_long(argc, argv, short_opts, long_opts, optc)) != -1)  // using getopt_long_only prevents optopt from being updated
    {
	/* some debug stuff */
	if (defs.debug)
	{
	    printf("optopt: %d\n", optopt); // optopt should be an integer. (should be the ascii representation of any unknown or misused option passed to this program)
	    printf("opterr: %d\n", opterr); // set to 1 to make getopt handle errors; set to 0 to disable getopt error-handling. 
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	}
	if (optopt != 0) // if there was a problem with the command-line usage, then
	{
	    error('A', 3);
	}

	switch(c)
	{
	    case 'h':
		help();
		return 0;
		break;

	    case 'v':
		printf("%s\n", version);
		return 0;
		break;

	}

    }
    
    /* some segfault prevention/error handling */

    if (argc == optind)
    {
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("if (argc == optind)\n");
	}

	error('A', 2);
    }
    else if (argc == 1)
    {
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("else if (argc == 1)\n");
	}
	error('A', 2);
    }
    /* if argv only contains command-line switches and this program's name, then */
    else if ((optind - 1) == argc) 
    {
	/* cut the power--that is exit non-zero! */
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("else if ((optind - 1) == argc)\n");
	}
	error('A', 2);
    }


    int argi; 

    /* create array Arga, and give it a length of argc - 1 
     *
     * (so as not to include the space this program's name takes up
     *
     * in argv) 
     *
     */
    char* arga[argc - 1];
    
    int auxi; // auxiliary argi

    auxi = 0;

    /* build arga */
    for (argi = optind; argi < argc; ++argi)
    {
	arga[auxi] = argv[argi];
	
	if (defs.debug)
	{
	    printf("argv[%d]: %s\n\n", auxi, argv[auxi]);
	    printf("arga[%d]: %s\n\n", auxi, arga[auxi]);
	    printf("auxi: %d\n\n", auxi);
	}
	auxi++;
    }
    if (defs.debug)
    {
	printf("argv[%d]: %s\n\n", auxi, argv[auxi]);
	printf("auxi: %d\n\n", auxi);

	int idex_0;
	
	for (idex_0 = auxi + 1; idex_0 < argc; idex_0++)
	{
	    printf("argv[%d]: %s\n\n", idex_0, argv[idex_0]);
	    printf("idex_0: %d\n\n", idex_0);
	}
    }
    int idex;
    int hdex;
    /* process arga one item at a time */ 
    for (hdex = 0; hdex < auxi; hdex++)
    {
	/* is argument string? (Yes, goto <pa40>. No, goto <pa20>.) */ 
	if (strlen(arga[hdex]) > 1)
	{
	    if (defs.debug)
	    {
		printf("string is longer than 1 character: %s\n\n", arga[hdex]);
	    }
	    char* acharlie;
	    /* process arga[hdex] one character at a time */  
	    for (acharlie = arga[hdex]; *acharlie; acharlie++)  // <pa40>
	    {
		idex = (int)*acharlie;
		printf("&#%d;", idex);
	    }
	}
	else // <pa20>
	{

	    idex = (int)*arga[hdex];
	    printf("&#%d;", idex);
	}
	printf("\n");
    }

  //  printf("\n"); // add a trailing newline



    return 0;
}
    
