/****
    (filename)
    
    opts.h

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.


****/


#ifndef OPTS_H_GUARD
#define OPTS_H_GUARD 1

#undef BEGIN_C_DECLS 
#undef END_C_DECLS
#ifdef __cplusplus
# define BEGIN_C_DECLS extern "C" {
# define END_C_DECLS }
#else
# define BEGIN_C_DECLS /* empty */
# define END_C_DECLS /* empty */
#endif 

#undef PARAMS
#if defined (__STDC__) || defined (_AIX) \
    || (defined (__mips) && defined (_SYSTYPE_SVR4))\
    || defined(WIN32) || defined(__cplusplus)
# define PARAMS(protos) protos
#else
# define PARAMS(protos) ()
#endif

BEGIN_C_DECLS 

//#include <getopt.h> //----------- getopt_long()

static const char* short_opts = "hv";

static const struct option long_opts[] =
{
    //{ char* name, int has_arg, int* flag, int val}
    { "debug", no_argument, &defs.debug, 1},
    { "help", no_argument, 0, 'h'},
    { "version", no_argument, 0, 'v'},
    { 0, 0, 0, 0} // necessary to prevent segfaults, I believe, and at any rate the GNU documentation for getopt says that this bit is required at one point.  
};

END_C_DECLS

#endif /* OPTS_H_GUARD */
