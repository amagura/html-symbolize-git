/****
    (filename)
    
    rmchar.c

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.

****/
#include "rmchar.h" //-------- self
#include <stdio.h> //--------- printf()
#include "defs_t.h" //------- defs.debug

char* rmchar(char* str, char c)
{
    char *dest, *src; /* destination string */

    src = dest = str;

    while (*src != '\0')
    {
	if (*src != c)
	{
	    *dest = *str;
	    dest++;
	}
	src++;
    }

    // reset destination string
    *dest = '\0';

    return str;
}

char* sfilter(char* str) // const char*, so that it can work with c_str()'s from c++ std::string's 
{
    char *dest, *src;

    src = dest = str;

    if (defs.debug)
    {
	printf("\n");
    }

    while (*src != '\0')
    {
	    if (defs.debug)
	    {
		printf("*dest-1: %c\n", *dest);
		printf("*str-1: %c\n", *str);
		printf("*src-1: %c\n\n", *src);
	    }
	if (*src != '&' && *src != '#' && *src != ';')
	{
	    *dest = *src;

	    if (defs.debug)
	    {
		printf("*dest-2: %c\n", *dest);
		printf("*str-2: %c\n", *str);
		printf("*src-2: %c\n\n", *src);
	    }
	    dest++;
	}
	src++;
    }

    *dest = '\0';
    
    if (defs.debug)
    {
	printf("\n");
    }

    return str;
}

char cfilter(char c)
{
    if (c == ';')
    {
	return '\0';
    }
    else if (c == '#')
    {
	return '\0';
    }
    else if (c == '&')
    {
	return '\0';
    }
    
    return c;
}
