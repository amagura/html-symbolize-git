/****
    (filename)
    
    unhsz.c

    (copyright)

    Copyright (c) 2013 Alexej Magura
    
    This file is part of Html SymboliZe.
    
    Html SymboliZe is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Html SymboliZe is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.
****/

// system includes
#include <stdio.h> //---------- printf()
#include <getopt.h> //--------- getopt_long()
#include <stdlib.h> //--------- exit(), free()
#include <stdbool.h> //-------- true, false
#include <string.h> //--------- strtok()

// local includes
#include "version.h" //-------- version
#include "help.h" //----------- help()
#include "error.h" //---------- error()
#include "defs_t.h" //--------- defs.debug, defs.program_name, defs.behavior
#include "rmchar.h" //--------- sfilter()
#include "opts.h" //----------- short_opts, long_opts[]

int main(int argc, char** argv)
{
    if (defs.debug)
    {
	printf("argc: %d\n", argc);
	printf("optind: %d\n", optind);
    }

    defs.debug = false;

    /* splint found a few memory leaks, this bit here addresses this */
    free(defs.program_name); 
    free(defs.behavior);
    free(defs.inversion);
    free(defs.inverba);

    defs.program_name = argv[0];
    defs.behavior = "decod";
    defs.inversion = "hsz";
    defs.inverba = "encod";

    /* variable for character storage */
    int c;

    /* opt counter */
    int* optc = 0;

    /* process args */

    while ((c = getopt_long(argc, argv, short_opts, long_opts, optc)) != -1)
    {
	/* some debug stuff */
	if (defs.debug)
	{
	    printf("optopt: %d\n", optopt); // optopt should be an integer. (should be the ascii representation of any unknown or misused option passed to this program)
	    printf("opterr: %d\n", opterr); // set to 1 to make getopt handle errors; set to 0 to disable getopt error-handling. 
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	}
	
	if (optopt != 0) // if there was a problem with the command-line usage, then
	{
	    error('A', 3);
	}

	switch(c)
	{
	    case 'h':
		help();
		return 0;
		break;

	    case 'v':
		printf("%s\n", version);
		return 0;
		break;

	}

    }
    
    /* some segfault prevention/error handling */

    if (argc == optind)
    {
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("if (argc == optind)\n");
	}

	error('A', 2);
    }
    else if (argc == 1)
    {
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("else if (argc == 1)\n");
	}
	error('A', 2);
    }
    /* if argv only contains command-line switches and this program's name, then */
    else if ((optind - 1) == argc) 
    {
	/* cut the power--that is exit non-zero! */
	if (defs.debug)
	{
	    printf("argc: %d\n", argc);
	    printf("optind: %d\n", optind);
	    printf("else if ((optind - 1) == argc)\n");
	}
	error('A', 2);
    }

    int argi;  

    char* arga[argc - 1];

    int auxi;  

    auxi = 0;

    size_t argalane = 0;

    for (argi = optind; argi < argc; ++argi)
    {
	arga[auxi] = argv[argi];

	argalane += strlen(argv[argi]);
	if (defs.debug)
	{
	    printf("strlen(argv[argi]): %d\n", (int)strlen(argv[argi]));
	}

	if (defs.debug)
	{
	    printf("argv[%d]: %s\n\n", auxi, argv[auxi]);
	    printf("arga[%d]: %s\n\n", auxi, arga[auxi]);
	    printf("auxi: %d\n\n", auxi);
	}
	auxi++;
    }
    if (defs.debug)
    {
	printf("argv[%d]: %s\n\n", auxi, argv[auxi]);
	printf("auxi: %d\n\n", auxi);
	
	int idex_0;

	for (idex_0 = auxi + 1; idex_0 < argc; idex_0++)
	{
	    printf("argv[%d]: %s\n\n", idex_0, argv[idex_0]);
	    printf("idex_0: %d\n\n", idex_0);
	}
    }

    int hdex;

    /* create an output storage buffer <sBp4> (section B, part 4)*/
    char* buf_it; 
    
    buf_it = *arga;
    
    if (defs.debug)
    {
	printf("buf_it: %s\n", buf_it);
    }
    
    char* acharlie = ""; // end of <sBp4>

    /* create a token storage buffer <sBp5> (section B, part 5) */
    char* token_buf; /* 
			end of <sBp5> */

    /* search input string for '#'s, '&'s, and ';'s

       processing it one character at a time */
/*
    for (idex = 0; idex < argc - optind; idex++)
    {
	if (defs.debug)
	{
	    printf("arga[%d]: %s\n", idex, arga[idex]);
	}
	*/
	/* split acharlie by ';' so that we can process
	   each html entity code individually */
//	while ((token_buf = strsep(&buf_it, ";")) != NULL)
	//{
    int count;

    if (strchr(buf_it, ';'))
    {
	count = 1; // at least 1
    }
    else
    {
	count = 0; // at least 0
    }

   for (hdex = 0; hdex < auxi; hdex++)
   {
        token_buf = strtok(buf_it, ";");

	int idex = 0; 

	while (token_buf != NULL)
	{
	    if (count == 0)
	    {
		error('A', 4);
	    }
	    
	    acharlie = sfilter(token_buf);
	    token_buf = strtok(NULL, ";");

	    int buf; 

	    buf = atoi(acharlie);

	    printf("%c", buf);

	    if (defs.debug)
	    {
		printf("token_buf: %s\n", token_buf);
		printf("acharlie-6: %s\n", acharlie);
		printf("buf: %d\n", buf);
		printf("buf-char: %c\n", buf);
		printf("acharlie-7: %s\n", acharlie);
	    }

	    idex++;

	}

	printf("\n"); // separate args by newlines
   }

    return 0;
}

