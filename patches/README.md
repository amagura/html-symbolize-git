# README

## Things you'll need

* GNU patch

* * *

## Details about the patchfiles and how to apply them

The `.*-maximize-enabled-.*` patchfiles re-enable the Maximize button in Html SymboliZe's GUI, version 2.0.1. 

(note: the Linux patchfile should also work on the Mac OS X release)

### Linux


> `patch xhsz.py xhsz-maximize-enabled-Linux.patch -u --verbose`

and to reverse the patch simply run: 

> `patch xhsz.py xhsz-maximize-enabled-Linux.patch -uR --verbose`

### Windows

> `patch hsz.py hsz-maximize-enabled-MSWin.patch -u --verbose`

and to reverse the patch, run:

> `patch hsz.py hsz-maximize-enabled-MSWin.patch -uR --verbose`
