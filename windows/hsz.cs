using System.Windows.Forms;
using System.Drawing; 
using System;

public class hsz : Form // aka, MainWindow
{
    public hsz()
    {
	/* Menu-bar setup START */

	MenuStrip ms = new MenuStrip();

	ms.Parent = this;

	ToolStripMenuItem _File = new ToolStripMenuItem("&File");

	    /* Quit-File-Menu-Item setup START */

	ToolStripMenuItem _quit = new ToolStripMenuItem("&Quit", null, new EventHandler(OnQuit));

	try {

	_quit.Image = Image.FromFile("icons/exit.png");

	}
	catch (Exception e) {

	    Console.WriteLine(e.Message);
	}
	    /* Quit-File-Menu-Item setup STOP */

	_quit.ShortcutKeys = Keys.Control | Keys.Q;
	
	_File.DropDownItems.Add(_quit);

	ms.Items.Add(_File);
	
	/* Menu-bar setup STOP */


	/* Window-Title-Text setup START */
	Text = "Html SymboliZe";
	/* Window-Title-Text setup STOP */

	/* Window-size setup START */
	Size = new Size(259, 206);
	/* Window-size setup STOP */

	/* Clear-button setup START */

	    // settings
	Button clear_bu = new Button();
	clear_bu.Location = new Point(30, 20);
	clear_bu.Text = "&Encode"; 
	
	    // events
//	    clear_bu.Click += new EventHandler()

	    // creation
	Controls.Add(clear_bu);

	/* Clear-button setup STOP */

	/* Icon setup START */ 

	try {
	    
	    // the icon's `expected location' is defined here
	    //						    ^--+
	    //						       |
							//     |
	    Icon = new Icon("icons/hsz.ico");		// <---+
	} 
	catch (Exception e) {
	    
	    Console.WriteLine(e.Message);
	}

	/* Icon setup STOP */

	CenterToScreen();
    }

    /* Event Methods START */

	// On Mouse Click
    void OnQuit(object sender, EventArgs e)
    {
	Close();
    }

    static public void Main()
    {
	/* Window creation START */
	Application.Run(new hsz());
	/* Window creation STOP */
    }
}
