#!/usr/bin/python2
# vim: set fileencoding=utf-8:

'''
	(filename)

	hsz.py

	(copyright)

	Copyright (c) 2013 Alexej Magura

	This file is part of Html SymboliZe.

	Html SymboliZe is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Html SymboliZe is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.
'''

# System imports
from re import sub
import wx 
from time import sleep


""" define a definitons storage class, where we can store generic information... like the current version and such. """

class defs:
	
	""" when set to true, enable debug mode """ 

	debug = False

	style = wx.DEFAULT_FRAME_STYLE

	no_max = False

class version:

	'returns the current version for Html SymboliZe'

	version = '2.0.4'

	def __init__(self):
		self.version = version.version
		self.debug = defs().debug


	def printout(self):
		if self.debug:
			return "version: %s" % self.version

		else :
			return self.version

class license:
	def __init__(self):

		self.license = \
				\
'''
Copyright (c) 2013 Alexej Magura

This file is part of Html SymboliZe

Html SymboliZe is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Html SymboliZe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Html SymboliZe.  If not, see <http://www.gnu.org/licenses/>.
'''
	def printout(self):
		return self.license

class encode:

	""" create a temporary storage area for the output string """

	str_buffer_e = ''

	def __init__(self, string_in):

		""" link up with the defs class' debug variable """

		self.debug = defs().debug 

		""" link up with this class' str_buffer E """

		self.str_buffer_e = encode.str_buffer_e

		""" make string_in accessible to anything and everything in class encode """

		self.string_in = string_in

		""" if we are in debug mode, then """
	
		if self.debug:

			""" print extras """
			
			print "encode-string_in: %s" % self.string_in

		""" for fruit in basket string_in """

		for fruit in range(len(self.string_in)):
			self.str_buffer_e += '&#%s;' % ord(self.string_in[fruit])


	def printout(self):
	
		""" return the contents of str_buffer E """

		return self.str_buffer_e 

class sfilter:

	""" create a temporary buffer for output of type string """

	str_buffer_f = ''

	def __init__(self, text_a):
		
		""" link arg passed to sfilter as 'text' to 'self.text' """

		self.text_a = text_a

		""" make str_buffer_f available to all methods in this class """

		self.str_buffer_f = sfilter.str_buffer_f

	def filter_all(self):

		""" I don't think that we need to do this, but I guess we will any way... create a temporary placeholder for the stuff returned by 're.sub' """

		str_buffer_g = ''


		""" if we are in debug mode """

		if defs.debug:

			""" print extras """

			print "sfilter.filter_all()-self.text_a: %s" % self.text_a # does print add a newline automatically?  I can't remember.

		str_buffer_g = sub(r'#', '', sub(r';', '', sub(r'&', '', self.text_a)))

		if defs.debug:
			print "sfilter.filter_all()-str_buffer_g: %s" % str_buffer_g

		""" for fruit in basket str_buffer_g, DO """

		for fruit in str_buffer_g:
			self.str_buffer_f += fruit # fruit, YUM :P

		if defs.debug:

			print "type(self.str_buffer_f: %s" % type(self.str_buffer_f)
			print "self.str_buffer_f: %s" % self.str_buffer_f

		""" return the contents of the str_buffer F """

		return self.str_buffer_f

	def replace_scolon_w_space(self):

		""" create yet another output string buffer thing """

		str_buffer_h = '' # uhhh... what charater are we at now...

		""" replace all occurances of ';' with ' ' """

		str_buffer_h = sub(r';', ' ', self.text_a)

		if defs.debug:
			print "sfilter.replace_scolon_w_space()-str_buffer_h: %s" % str_buffer_h

		""" return the result """

		return str_buffer_h

class decode: # NOTE, I somehow have this mystic power where I can clear the text area if my corresponding button is pressed twice, this was originally a FIXME, but I kind of like it this way.  
		# NOTE, _Please_ _don't_ _change_ _it_.

	""" create a temporary storage buffer to store output in """

	str_out_buffer_i = '' # oh, maybe this is why it clears the text area after two presses of the 'decode' button, then again... maybe not.

	""" create a temporary storage buffer to store output in of type list """

	list_out_buffer_c = ['']

	""" create a temporary storage buffer to store output in of type string """

	str_out_buffer_l = ''

	def __init__(self, text_b):

		""" if we are in debug mode, then """

		if defs.debug:
			
			""" print extras """

			print "decode.str_out_buffer_l: %s" % decode.str_out_buffer_l
			print "decode.list_out_buffer_c: %s" % decode.list_out_buffer_c
			print "decode.str_out_buffer_i: %s" % decode.str_out_buffer_i

		""" link up with the sfilter class so that we can use its powers of might and magic, and pass the arg that was passed to this class to the sfilter class """
		self.sfilter = sfilter(text_b)

		self.str_out_buffer_i = decode.str_out_buffer_i


		""" set self.text_b to the return status of self.sfilter.replace_scolon_w_space() """

		self.text_b = self.sfilter.replace_scolon_w_space()
		
		""" redefine self.sfilter so that it will use the output of sfilter.replace_scolo_w_space() """
		
		self.sfilter = sfilter(self.text_b)
		
		""" set str_out_buffer_i to the return status of self.sfilter.filter_all(), which should return a processed version of text_b """

		self.str_out_buffer_i = self.sfilter.filter_all()

		""" link up with decode's str_out_buffer_l """

		self.str_out_buffer_l = decode.str_out_buffer_l

		""" time to process this stuff, fool """

		for fruit in self.str_out_buffer_i:
			
			""" if current item is not a space, then """

			if fruit is not ' ':
				self.str_out_buffer_l += '%s' % fruit 
				continue 

			elif fruit is ' ':
				
				""" else, if current item IS a space, then """
				
				""" append the current contents of str_out_buffer_l to the list_out_buffer_C """

				self.list_out_buffer_c += [self.str_out_buffer_l]

				""" reset the value of str_out_buffer_l """

				self.str_out_buffer_l = decode.str_out_buffer_l

			else : 
				self.str_out_buffer_l += ', '
				continue

			if defs.debug:
				print "decode-fruit: %s" % fruit
				print "decode-self.list_out_buffer_c[1:]: %s" % self.list_out_buffer_c[1:]

			for fruit in self.list_out_buffer_c[1:]:
				self.str_out_buffer_l += unichr(int(fruit))

	def printout(self):
		return self.str_out_buffer_l



stockUndo = []
stockRedo = []

class MainWindow(wx.Frame):
	def __init__(self, parent, id, title, style=wx.DEFAULT_FRAME_STYLE):

		wx.Frame.__init__(self, parent, id, title=title, size=(259, 206), style=style)

		panel = wx.Panel(self, -1)

		ico = wx.Icon('icons/hsz.ico', wx.BITMAP_TYPE_ICO) # reqs icons/hsz.ico
		self.SetIcon(ico)

		sizer = wx.GridBagSizer(4, 4)

		self.statusbar = self.CreateStatusBar() # status bar at bottom of window
		self.statusbar.SetStatusText('Ready')

		# setting up the window menu
		filemenu = wx.Menu()
		editmenu = wx.Menu()
		aboutmenu = wx.Menu()
		debugmenu = wx.Menu()
		self.uIndex = 1
		self.rIndex = 0

		# wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
		m_About = aboutmenu.Append(wx.ID_ABOUT, "&About", "Information about this program.")
		m_Bugs = aboutmenu.Append(wx.ID_ANY, "&Contact Info", "Contact information.")

		m_Debug = aboutmenu.AppendMenu(wx.ID_HELP, '&Debug', debugmenu)
		self.m_Stat = debugmenu.Append(wx.ID_ANY, 'Show &Stats\tCtrl+S', 'Print stats to standard output', kind=wx.ITEM_CHECK)
		m_StatUp = debugmenu.Append(wx.ID_ANY, '&Update Stats\tCtrl+U', 'Reprint stats to stdout')
		m_Exit = filemenu.Append(wx.ID_EXIT, "&Quit\tCtrl+Q", "Quit program.")


		# undo, redo
		self.m_Undo = editmenu.Append(wx.ID_UNDO, "&Undo\tCtrl+Z", "Undo changes.")
		self.m_Redo = editmenu.Append(wx.ID_REDO, "&Redo\tCtrl+Shift+Z", "Redo changes.")
		editmenu.AppendSeparator()
		self.m_Sall = editmenu.Append(wx.ID_SELECTALL, "Select &all\tCtrl+A", 'Select all text present in text area.')
		self.m_Snone = editmenu.Append(wx.ID_ANY, "Select &none\tCtrl+Shift+A", 'Deselect text present in text area.')
		editmenu.AppendSeparator()

		# clear
		self.m_Clear = editmenu.Append(wx.ID_CLEAR, "C&lear\tCtrl+M", "Clear text area.")
		editmenu.AppendSeparator()
		# Cut, copy, and paste
		self.m_Cut = editmenu.Append(wx.ID_CUT, "Cu&t\tCtrl+X", "Cut from text area.")
		self.m_Copy = editmenu.Append(wx.ID_COPY, "Cop&y\tCtrl+C", "Copy from text area.")
		self.m_Paste = editmenu.Append(wx.ID_PASTE, "&Paste\tCtrl+V", "Paste into text area.")
		self.m_Redo.Enable(False)

		
		
		# create the menubar
		menuBar = wx.MenuBar()
		menuBar.Append(filemenu, "&File")
		menuBar.Append(editmenu, "&Edit")
		menuBar.Append(aboutmenu, '&Help')

		self.SetMenuBar(menuBar)

		# events
		self.Bind(wx.EVT_MENU, self.OnStat, self.m_Stat)
		self.Bind(wx.EVT_MENU, self.OnUpStat, m_StatUp)
		
		self.Bind(wx.EVT_MENU, self.OnAbout, m_About)
		
		self.Bind(wx.EVT_MENU, self.OnExit, m_Exit)
		
		self.Bind(wx.EVT_MENU, self.OnUndo, self.m_Undo)
		self.Bind(wx.EVT_MENU, self.OnRedo, self.m_Redo)

		self.Bind(wx.EVT_MENU, self.OnClear, self.m_Clear)

		self.Bind(wx.EVT_MENU, self.OnCut, self.m_Cut)

		self.Bind(wx.EVT_MENU, self.OnCopy, self.m_Copy)

		self.Bind(wx.EVT_MENU, self.OnPaste, self.m_Paste)

		self.Bind(wx.EVT_MENU, self.OnSall, self.m_Sall)

		self.Bind(wx.EVT_MENU, self.OnSnone, self.m_Snone)

		self.Bind(wx.EVT_MENU, self.OnBugs, m_Bugs)

		# text control area 
		self.tc = wx.TextCtrl(panel, style=wx.TE_MULTILINE, pos=(0, 0), size=(253, 62))

		#self.st = wx.StaticText(panel, label='%s\n%s' % (self.uIndex, self.rIndex), style=wx.ALIGN_LEFT)
		# buttons
		self.b_encode = wx.Button(panel, label="E&ncode", pos=(85, 66), size=(75, 62))
		self.b_decode = wx.Button(panel, label="&Decode", pos=(166, 66), size=(75, 62))
		self.Bind(wx.EVT_BUTTON, self.OnE, self.b_encode)
		self.Bind(wx.EVT_BUTTON, self.OnD, self.b_decode)

		self.b_clear = wx.Button(panel, wx.ID_CLEAR, "Clear", pos=(6, 66))
		self.Bind(wx.EVT_BUTTON, self.OnClear, self.b_clear)

		sizer.Add(self.tc, pos=(0, 0), span=(3, 3), flag=wx.EXPAND, border=6)
		sizer.AddGrowableCol(1)
		sizer.AddGrowableRow(0)
		sizer.AddGrowableRow(1)
		sizer.AddGrowableCol(2)
		sizer.Add(self.b_clear, pos=(3, 0), flag=wx.LEFT, border=6)
		sizer.Add(self.b_encode, pos=(3, 1), flag=wx.EXPAND|wx.RIGHT, border=2)
		sizer.Add(self.b_decode, pos=(3, 2), flag=wx.EXPAND|wx.RIGHT, border=12)

		panel.SetSizer(sizer)

		# initial value for stockUndo
		stockUndo.append(self.tc.GetValue())

		self.Show(True)

	def OnCut(self,e):
		dataObj = wx.TextDataObject()
		if defs.debug:
			print self.tc.Selection
		if self.tc.Selection[0] is self.tc.Selection[1]:
			dataObj.SetText(self.tc.GetValue())
			self.tc.SetValue('')
		else :
			if defs().debug:
				print self.tc.Cut()
			else :
				self.tc.Cut()
	def OnSall(self,e):
		if defs().debug:
			print "OnSall: self.tc.SelectAll(): %s" % self.tc.SelectAll()

		self.tc.SelectAll()

	def OnSnone(self,e):
		if defs.debug:
			print "OnSnone: self.tc.SetSelection(0, 0): %s" % self.tc.SetSelection(0, 0)
		self.tc.SetSelection(0, 0)

	def OnBugs(self,e):
		dlg = wx.MessageDialog(self, "I can be reached at lspci.sh@gmail.com; if you have patches or bug reports, please send them to this address.", "Contact Info", wx.OK)
		dlg.ShowModal()
		dlg.Destroy()

	def OnCopy(self,e):
		dataObj = wx.TextDataObject()
		if self.tc.Selection[0] is self.tc.Selection[1]:
			dataObj.SetText(self.tc.GetValue())
		else :
			dataObj.SetText(self.tc.GetStringSelection())

		if wx.TheClipboard.Open():
			wx.TheClipboard.SetData(dataObj)
			wx.TheClipboard.Close()
		else :
			wx.MessageBox("Unable to open clipboard", "Error")
	def OnPaste(self,e):
		dataObj = wx.TextDataObject()
		if wx.TheClipboard.Open():
			wx.TheClipboard.GetData(dataObj)
			wx.TheClipboard.Close()
			self.tc.SetValue(dataObj.GetText())

	def OnStat(self,e):
		if defs.debug:
			print 'e: %s' % e
		if not self.m_Stat.IsChecked():
			defs.debug = False
		else :
			defs.debug = True
			print 'b_encode.GetPosition(): %s' % self.b_encode.GetPosition()
			print 'b_decode.GetPosition(): %s' % self.b_decode.GetPosition()
			print 'b_clear.GetPosition(): %s' % self.b_clear.GetPosition()
			print 'self.tc.GetPosition(): %s' % self.tc.GetPosition()
			print 'self.GetPosition(): %s\n' % self.GetPosition()
			print 'b_encode.GetSize(): %s' % self.b_encode.GetSize()
			print 'b_decode.GetSize(): %s' % self.b_decode.GetSize()
			print 'b_clear.GetSize(): %s' % self.b_clear.GetSize()
			print 'self.tc.GetSize(): %s' % self.tc.GetSize()
			print 'self.GetSize(): %s\n' % self.GetSize()

	def OnUpStat(self,e):

		self.m_Stat.Check(True)
		self.OnStat(e)

	# OnAbout 
	def OnAbout(self,e):
		# a message dialog box with an OK button.  wx.OK is the standard, I believe.  
		info = wx.AboutDialogInfo()
		info.SetName('Html SymboliZe')
		info.SetVersion('%s' % self.Version())
		info.SetCopyright('(C) 2013 Alexej Magura')
		info.SetWebSite('http://sourceforge.net/projects/htmlsymbolize/')
		info.AddDeveloper('Alexej Magura')
		info.SetLicense('%s' % self.License())
		wx.AboutBox(info)

	# OnProcess
	def Processing(self, mode):
		if mode is 1 or mode is True:
			self.statusbar.SetStatusText('Processing...')
			if defs.debug:
				print 'mode: %s' % 1
		elif mode is 0 or mode is False:
			self.statusbar.SetStatusText('Ready')
			if defs.debug:
				print 'mode: %s' % 0
		else :
			self.statusbar.SetStatusText('Ready')

	# OnExit
	def OnExit(self,e):
		self.Close(True) # Close the frame, what else.  

	def OnUndo(self,e):
		self.Processing(1)
		if defs.debug:
			print 'stockUndo: %s\n' % stockUndo
		self.Add2Redo(self.tc.GetValue())
		self.rIndex += 1
		if len(stockUndo) is 1:
			self.tc.SetValue(stockUndo[0])
			self.m_Undo.Enable(False)
			self.uIndex -= 1
		else :
			self.tc.SetValue(stockUndo.pop())
			self.uIndex -= 1
		self.Processing(0)

	def OnRedo(self,e):
		self.Processing(1)
		if defs.debug:
			print 'stockRedo: %s\n' % stockRedo
		self.Add2Undo(self.tc.GetValue())
		if len(stockRedo) is 1:
			self.tc.SetValue(stockRedo[0])
			self.m_Redo.Enable(False)
		elif len(stockRedo) is 0:
			self.m_Redo.Enable(False) # do nothing
		else :
			self.tc.SetValue(stockRedo.pop())
		self.Processing(0)

	def Add2Redo(self, what):
		stockRedo.append(what)
		if self.m_Redo.IsEnabled() is False:
			self.m_Redo.Enable(True)


	def Add2Undo(self, what):
		stockUndo.append(what)
		if self.m_Undo.IsEnabled() is False:
			self.m_Undo.Enable(True)

	# On Encode
	def OnE(self,e):

		""" set the status bar to read 'Processing...' """

		self.Processing(1)
		
		""" add the current value of the text area to the StockExch--I mean, StockUndo :P """

		self.Add2Undo(self.tc.GetValue())

		""" create a class instance of encode and pass the current value of the text area to that class instance """

		ci_d = encode(self.tc.GetValue())

		""" set the current value of the text area to the value returned by our class instance's printout """

		self.tc.SetValue(ci_d.printout())

		""" update the status bar to reflect the fact that we are no longer processing the user's desired transaction """

		self.Processing(0)

	# On Decode
	def OnD(self,e):
		
		""" set the status bar to read 'Processing...' """

		self.Processing(1)
		
		""" add the current value of the text area to the StockUndo list """

		self.Add2Undo(self.tc.GetValue())

		""" create a class instance of the decode class and pass the current value of the text area to said class instance """

		ci_b = decode(self.tc.GetValue())

		""" create a temporary storage buffer of type string """

		str_buffer_d = ''

		""" create a temporary storage buffer of type string """

		str_buffer_c = ''

		""" if debug mode is on """

		if defs.debug :

			""" print extras :P """

			print "OnD-ci_b.printout(): %s" % ci_b.printout() # FIXME, I only return integers!!!!

		""" for monkeys in barrel class_instance_B """

		for items in ci_b.printout(): 
			
			""" if debug mode is enabled """

			if defs.debug:
				
				""" print the monkey!!! """

				print 'OnD-items: %s' % items 

			""" append the current item to string_buffer_D """

			str_buffer_d += items

			if defs.debug:

				print 'OnD-str_buffer_d: %s' % str_buffer_d
				print "OnD-str_buffer_d-3: %s%s%s" % ("`", str_buffer_d, "'")

			""" uh... if current item equals a single space, then... """

			if items == ' ': # why are we looking for a space here?  

				""" cast str_buffer_d to integer and then cast that sucker into unicode-characters, oh and add the now converted stuff to str_buffer_c """
		
				if defs.debug:
					print "OnD-str_buffer_c: %s" % str_buffer_c
				if str_buffer_d != ' ':
					str_buffer_c += unichr(int(str_buffer_d))  

				if defs.debug:
					print "OnD-str_buffer_c: %s" % str_buffer_c

				""" reset the value of str_buffer_d back to void """

				str_buffer_d = ''

				""" begin the next iteration of the loop """

				continue

		""" lastly set the current value of the text area to the contents of str_buffer_c """

		if defs.debug:
			print "OnD-self.tc.GetValue(): %s" % self.tc.GetValue()


		self.tc.SetValue(str_buffer_c)
		
		if defs.debug:
			print "OnD-self.tc.GetValue()-2: %s%s%s" % ("`", self.tc.GetValue(), "'")
		
		""" update the status bar """

		self.Processing(0)

	# On Clear
	def OnClear(self,e):

		""" add the current value of the text area to the undolist, so that the user can 'undo' clearing the text area """
		
		self.Add2Undo(self.tc.GetValue())

		if defs.debug:
			print "OnClear-self.tc.GetValue(): %s" % self.tc.GetValue() 
		
		""" enable the 'Undo' menu-button-thingy in the Edit menu """
		
		self.m_Undo.Enable(True) 

		""" clear the text area by setting its value to NURN, I mean NULL.  :P """

		self.tc.SetValue('')

	def License(self):
		
		""" create an instance of the license class """
		
		ci_c = license()

		""" return whatever the printout method from our instance of the license class returns """

		return ci_c.printout()

	def Version(self):
		ci_a = version() # ci stands for Class_Instance
		return ci_a.printout()

class App(wx.App):
	def OnInit(self):
		if defs.no_max is True:
			frame = MainWindow(None, -1, "Html SymboliZe", wx.DEFAULT_FRAME_STYLE - wx.MAXIMIZE_BOX)
		else :
			frame = MainWindow(None, -1, "Html SymboliZe")

		frame.Show(True)
		frame.Center()
		return True


def main():

	from sys import stderr, exit, argv

	import getopt

	try: 

		options, arguments = getopt.getopt(argv[1:], "hnv", ["demaximize", "help", "version"])

	except getopt.GetoptError as e:

		print "%s: %s" % ( argv[0], e)

		exit(2)

		
	for c, optarg in options:

		if c in ('-h', '--help'):

			print "usage: %s [options]" % argv[0]
			print 
			print "  -h, --help\t\tprint this message and exit"
			print "  -v, --version\t\tprint version and exit"
			print "  -n, --demaximize\tdisable maximize button (your mileage may vary)"

			exit(0)

		elif c in ('-v', '--version'):

			print version().printout()

			exit(0)

		elif c in ('-n', '--demaximize'):

			defs.no_max = True
			



if __name__ == '__main__':
	main()
	app = App(0)
	app.MainLoop()

