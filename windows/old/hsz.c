#include <gtk/gtk.h> // Gtk, duh :P
#include <gdk-pixbuf/gdk-pixbuf.h> // GdkPixBuf...
#include <gdk/gdkkeysyms.h> // GtkAccelGroup, I think
#include <stdbool.h> //---------------------------------- false


GdkPixbuf* create_pixbuf(const gchar* filename)
{
    GdkPixbuf* pixbuf;
    GError* error = NULL;
    pixbuf = gdk_pixbuf_new_from_file(filename, &error);

    if ( !pixbuf)
    {
	fprintf(stderr, "%s\n", error->message);
	g_error_free(error);
    }

    return pixbuf;
}

int main(int argc, char** argv)
{
    GtkWidget* window;

    /* Sizer declaration START */
    GtkWidget* vbox; // need three of these
    GtkWidget* vbox_2; 
    GtkWidget* vbox_3;
    /* Sizer declaration STOP */

    /* Menubar declaration START */
    GtkWidget* menubar;
    /* Menubar declaration STOP */

    /* Menubar setup START */
    
	/* File-menu declaration START */
    GtkWidget* _filemenu;
    GtkWidget* _file;
    GtkWidget* _quit;
	/* File-menu declaration STOP */

	/* Keybinds declaration START */

    GtkAccelGroup* accel_group = NULL;

	/* Keybinds declaration STOP */


    gtk_init(&argc, &argv); // lets gtk receive command-line options

    /* Window setup START */
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL); 
    
    gtk_window_set_title(GTK_WINDOW(window), "Html SymboliZe"); 
    
    gtk_window_set_default_size(GTK_WINDOW(window), 259, 201); 
    
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);

    gtk_window_set_icon(GTK_WINDOW(window), create_pixbuf("icons/hsz.ico"));

    /* Window setup STOP */

    /* Sizers creation and setup START */
    vbox = gtk_vbox_new(false, 0);
    gtk_container_add(GTK_CONTAINER(window), vbox);
    /* Sizers creation and setup STOP */

    /* Menu(s|bar) creation START */
    menubar = gtk_menu_bar_new();
    _filemenu = gtk_menu_new();





    /* Window what'll-show-up-setup START */
    gtk_widget_show(window);
    /* Window what'll-show-up-setup STOP */

    g_signal_connect_swapped(G_OBJECT(window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

    /* Window creation START */
    gtk_main();
    /* Window creation STOP */

    /* Exit status START */
    return 0;
    /* Exit status STOP */

}


