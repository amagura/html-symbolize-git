#!/usr/bin/perl 

use warnings; 

my $nil = undef;

package Html_SymboliZe;

use base 'Wx::App';

sub OnInit {

    my $self = shift;

    my $frame = wx::Frame->new(
	$nil,
	'Html SymboliZe',
	&Wx::wxDefaultPosition,
	&Wx::WxDefaultSize,
	&Wx::wxMAXIMIZE_BOX | &Wx::wxCLOSE_BOX
    );

    $frame->Show;
}

MyApp->new->MainLoop;

